# target: all - Default target. Does nothing.
all:
	echo "Hello $(LOGNAME), nothing to do by default"
	# sometimes: echo "Hello ${LOGNAME}, nothing to do by default"
	echo "Try 'make help'"

# target: help - Display callable targets.
help:
	egrep "^# target:" [Mm]akefile

# target: install - Install files and enable services
install:
	# copy nginx files
	htpasswd -c {{cookiecutter.proxy_auth_file}} {{cookiecutter.proxy_auth_user}}
	cp nginx/nginx.site /etc/nginx/sites-enabled/{{cookiecutter.proxy_server_name}}.site
	# copy update script
	mkdir -p /usr/local/sbin
	cp sbin/update-1984-rtmp-conf /usr/local/sbin
	# copy template file
	mkdir -p /var/lib/1984-proxy
	cp -r tmpl /var/lib/1984-proxy
	# setup conf watcher
	cp systemd/1984-rtmp-conf-watcher.* /etc/systemd/system
	systemctl daemon-reload
	systemctl enable 1984-rtmp-conf-watcher.path
	systemctl start 1984-rtmp-conf-watcher.path
	# setup ping watcher
	cp systemd/1984-ping-watcher.* /etc/systemd/system
	systemctl daemon-reload
	systemctl enable 1984-ping-watcher.path
	systemctl start 1984-ping-watcher.path
	# setup genxspf timer
	cp systemd/1984-genxspf-{{cookiecutter.genxspf_stream_name}}.* /etc/systemd/system
	systemctl daemon-reload
	systemctl enable 1984-genxspf-{{cookiecutter.genxspf_stream_name}}.timer
	systemctl start 1984-genxspf-{{cookiecutter.genxspf_stream_name}}.timer

# target: uninstall - Disable services and uninstall files
uninstall:
	# clean genxspf timer
	systemctl stop 1984-genxspf-*.timer 				|| true
	systemctl disable 1984-genxspf-*.timer 				|| true
	rm -rf /etc/systemd/system/1984-genxspf-*
	# clean ping watcher
	systemctl stop 1984-ping-watcher.path 				|| true
	systemctl disable 1984-ping-watcher.path			|| true
	cp /etc/systemd/system/1984-ping-watcher.*
	# clean conf watcher
	systemctl stop 1984-rtmp-conf-watcher.path			|| true
	systemctl disable 1984-rtmp-conf-watcher.path		|| true
	cp /etc/systemd/system/1984-rtmp-conf-watcher.*
	# clean template file
	rm -rf /var/lib/1984-proxy
	# clean update script
	rm /usr/local/sbin/update-1984-rtmp-conf 			|| true
	# clean nginx files
	rm /etc/nginx/sites-enabled/{{cookiecutter.proxy_server_name}}.site	|| true
