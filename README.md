# CookieCutter Template For 1984-Proxy Setup

## Generate

Generate the project tree:

```bash
$ cookiecutter gl:toopy/cookiecutter-1984-proxy
picam_user [?]: ?
```

## Install

Install files and start services

```bash
$ sudo make -C 1984-proxy install
# install requirements
...
systemctl restart nginx
```

## Gotcha!

Video links:

```bash
$ xdg-open http://foo:bar@rtmp.proxy/hls.html?name=test
$ cvlc rtmp://rtmp.proxy:1935/live/test
$ vlc http://foo:bar@rtmp.proxy/flv/test.xspf
```

Motion links:

```bash
$ xdg-open http://foo:bar@rtmp.proxy/motion.html?name=test
$ xdg-open http://foo:bar@rtmp.proxy/motion_data?name=test
```

Status link:

```bash
$ xdg-open http://foo:bar@rtmp.proxy/status?name=test
```

## LICENSE

MIT
